import { Ramal } from "../models";

export type RamalProductivityData = {
  ramal?: Ramal,
  timeSlice?: {
      [time: string]: {
          amount?: number,
          accumulated?: boolean,
          order?: number
      }
  }
}
