export type RamalProductivityReportInput = {
  contactListId?:string,
  accumulatedTimeSlicesInMinutes?: number[],
  dates?: string[]
  formId?:number
}
