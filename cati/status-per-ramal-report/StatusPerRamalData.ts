import { HistoryStatus } from "../models";

export type StatusPerRamalData = {
    ramal?: {
        ramalName?:string,
        ramalNumber?:string
    },
    status?: HistoryStatus,
    timeSlice?: {
        [time: string]: {
            amount?: number,
            accumulated?: boolean,
            order?: number
        }
    }
}
