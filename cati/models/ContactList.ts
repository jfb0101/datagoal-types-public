import { Contact } from "./Contact"

export class ContactList {

    constructor(data:Partial<ContactList>) {
        Object.assign(this,data)
    }

    public id?:string
    public formId?:number
    public fileName?:string
    public date?:Date
    public deleted?:boolean
    public schedulingOnlyForThoseWhoScheduled?:boolean
    public contacts?:Contact[]
    public maxRetriesPerContact?:number
    public minRetryInterval?:number
    public filter?:any
    public typeOfFilter?: "PRIORITY" | "EXCLUSIVITY"
    public validUntil?:Date
    public disabled?:boolean
    public name?:string
    public isForAutomaticCall?:boolean
    public toJSON?: () => ContactList
}