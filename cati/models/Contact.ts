import { ContactList } from "./ContactList"
import { ContactHistory } from "./ContactHistory"

export class Contact {
    constructor(data:Partial<Contact>) {
        Object.assign(this,data)
    }

    public uuid__?: string
    public contactListId?:string
    public contactList?:ContactList
    public history?: ContactHistory[]
    public phoneToCall?:string
    public link?:string
    public id?:string

    [key:string]: any
}
