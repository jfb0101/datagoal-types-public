import { HistoryStatus } from "./HistoryStatus"

export class ContactHistory {

    constructor(data:Partial<ContactHistory>) {
        Object.assign(this,data)
    }

    status?: HistoryStatus
    date?: Date
    schedulingDateTime?:Date
    uuid?:string
    phone?:string
    userId?:string
    ramal?:string
    observation?:string
    indicatedFromContactUUID?:string
    [key:string] : any

}
