import { Contact } from "../Contact";

export namespace ContactRemoteMethodsTypes {
    export type CreateInput = {
        contact:Contact
        indicatedFromContactUUID?:string,
        observation?:string
        userId?:string
        ramal?:string
        isSingleContact?:boolean
    }
}