import { HistoryStatus } from "./HistoryStatus"

export class LastContactHistory {
    public date:Date
    public status:HistoryStatus
    public contactUUID:string
    public historyUUID:string
    public userId:string
}