export class ContactStatusData {
    constructor(data: Partial<ContactStatusData>) {
        Object.assign(this,data)
    }

    schedulingDateTime?:Date
    phone?:string
    userId?:string
    ramal?:string
}