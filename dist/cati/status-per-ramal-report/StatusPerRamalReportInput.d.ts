export type StatusPerRamalReportInput = {
    contactListId?: string;
    accumulatedTimeSlicesInMinutes?: number[];
    dates?: string[];
    formId?: number;
};
