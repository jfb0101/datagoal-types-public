"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Contact = void 0;
var Contact = /** @class */ (function () {
    function Contact(data) {
        Object.assign(this, data);
    }
    return Contact;
}());
exports.Contact = Contact;
