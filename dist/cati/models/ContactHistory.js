"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactHistory = void 0;
var ContactHistory = /** @class */ (function () {
    function ContactHistory(data) {
        Object.assign(this, data);
    }
    return ContactHistory;
}());
exports.ContactHistory = ContactHistory;
