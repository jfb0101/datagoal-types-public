"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactStatusData = void 0;
var ContactStatusData = /** @class */ (function () {
    function ContactStatusData(data) {
        Object.assign(this, data);
    }
    return ContactStatusData;
}());
exports.ContactStatusData = ContactStatusData;
