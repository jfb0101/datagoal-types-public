"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HistoryStatus = void 0;
exports.getAllHistoryStatus = getAllHistoryStatus;
var HistoryStatus;
(function (HistoryStatus) {
    HistoryStatus["CREATED"] = "CREATED";
    HistoryStatus["IN_CALL"] = "IN_CALL";
    HistoryStatus["ANSWERED"] = "ANSWERED";
    HistoryStatus["NOT_ANSWERED"] = "NOT_ANSWERED";
    HistoryStatus["BUSY"] = "BUSY";
    HistoryStatus["REFUSAL_TO_PARTICIPATE"] = "REFUSAL_TO_PARTICIPATE";
    HistoryStatus["INVALID_PHONE"] = "INVALID_PHONE";
    HistoryStatus["SCHEDULING"] = "SCHEDULING";
    HistoryStatus["HARVEST_NOT_COMPLETED"] = "HARVEST_NOT_COMPLETED";
    HistoryStatus["HARVEST_COMPLETED"] = "HARVEST_COMPLETED";
    HistoryStatus["ABANDONED"] = "ABANDONED";
    HistoryStatus["INDICATED_ANOTHER_CONTACT"] = "INDICATED_ANOTHER_CONTACT";
    HistoryStatus["INDICATED_FROM_EXISTING_CONTACT"] = "INDICATED_FROM_EXISTING_CONTACT";
    HistoryStatus["HARVEST_DENIED"] = "HARVEST_DENIED";
})(HistoryStatus || (exports.HistoryStatus = HistoryStatus = {}));
function getAllHistoryStatus() {
    return Object.keys(HistoryStatus).map(function (i) { return HistoryStatus[i]; });
}
