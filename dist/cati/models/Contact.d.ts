import { ContactList } from "./ContactList";
import { ContactHistory } from "./ContactHistory";
export declare class Contact {
    constructor(data: Partial<Contact>);
    uuid__?: string;
    contactListId?: string;
    contactList?: ContactList;
    history?: ContactHistory[];
    phoneToCall?: string;
    link?: string;
    id?: string;
    [key: string]: any;
}
