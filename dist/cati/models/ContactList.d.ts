import { Contact } from "./Contact";
export declare class ContactList {
    constructor(data: Partial<ContactList>);
    id?: string;
    formId?: number;
    fileName?: string;
    date?: Date;
    deleted?: boolean;
    schedulingOnlyForThoseWhoScheduled?: boolean;
    contacts?: Contact[];
    maxRetriesPerContact?: number;
    minRetryInterval?: number;
    filter?: any;
    typeOfFilter?: "PRIORITY" | "EXCLUSIVITY";
    validUntil?: Date;
    disabled?: boolean;
    name?: string;
    isForAutomaticCall?: boolean;
    toJSON?: () => ContactList;
}
