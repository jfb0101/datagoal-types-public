import { HistoryStatus } from "./HistoryStatus";
export declare class ContactHistory {
    constructor(data: Partial<ContactHistory>);
    status?: HistoryStatus;
    date?: Date;
    schedulingDateTime?: Date;
    uuid?: string;
    phone?: string;
    userId?: string;
    ramal?: string;
    observation?: string;
    indicatedFromContactUUID?: string;
    [key: string]: any;
}
