import { HistoryStatus } from "./HistoryStatus";
export declare class LastContactHistory {
    date: Date;
    status: HistoryStatus;
    contactUUID: string;
    historyUUID: string;
    userId: string;
}
