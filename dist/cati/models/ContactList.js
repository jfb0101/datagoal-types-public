"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactList = void 0;
var ContactList = /** @class */ (function () {
    function ContactList(data) {
        Object.assign(this, data);
    }
    return ContactList;
}());
exports.ContactList = ContactList;
