import { Contact } from "../Contact";
export declare namespace ContactRemoteMethodsTypes {
    type CreateInput = {
        contact: Contact;
        indicatedFromContactUUID?: string;
        observation?: string;
        userId?: string;
        ramal?: string;
        isSingleContact?: boolean;
    };
}
