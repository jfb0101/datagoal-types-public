export declare class ContactStatusData {
    constructor(data: Partial<ContactStatusData>);
    schedulingDateTime?: Date;
    phone?: string;
    userId?: string;
    ramal?: string;
}
